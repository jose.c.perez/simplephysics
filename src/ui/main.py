import tkinter as tk
from tkinter import ttk

root = tk.Tk()
root.title("simplephysics")

# Tabs
tabControl = ttk.Notebook(root)
tab1 = ttk.Frame(tabControl)
tabControl.add(tab1, text ='Young Module')
tabControl.pack(expand = 1, fill ="both")


# Young
youngLabel = tk.Label(tab1, text="Y:", justify=tk.LEFT, relief="solid")
youngEntry = tk.Entry(tab1, relief="solid")

# sigma
sigmaLabel = tk.Label(tab1, text="d:", justify=tk.LEFT, relief="solid")
sigmaEntry = tk.Entry(tab1, relief="solid")





myLabel = tk.Label(tab1, text="Fill up values and we will try to calculate")
myLabel.grid(row=0, column=0, columnspan=2)
youngLabel.grid(row=1, column=0, sticky="e")
youngEntry.grid(row=1, column=1, padx=20)
sigmaLabel.grid(row=2, column=0, sticky="e")
sigmaEntry.grid(row=2, column=1, padx=20)


root.mainloop()